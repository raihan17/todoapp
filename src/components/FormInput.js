import React, { Component } from 'react';
import Button from './Button';

import "../styles/FormInput.css";

const inputForm = {
    background: "#fff",
    color: "#fff",
    display: "flex",
    alignItems: "center",
    height: "3rem",
    padding: "0 1rem",
    justifyContent: "space-between",
    margin: "0.5rem 0",
    borderRadius: "10px",
    "focus":{
        boxShadow: "0 0 0 0.2rem rgba(0, 123, 255, 0.25)"
    }
}

const inputText = {
    width: "70%",
    border: "none",
}

class FormInput extends Component {
    // membuat state awal untuk value form inputan yang mana state.text = ""
    state = {
        text: ""
    }
    change = e => {
        this.setState({ text: e.target.value })
    }
    submit = e => {
        e.preventDefault();
        if(this.state.text !== "") {
            this.props.add(this.state.text);
            alert('Berhasil..')
        }
        this.setState({
            text: ""
        })
    }

    render() {
        return (
            <form style = { inputForm } onSubmit = { this.submit } >
                <input 
                    type="text"
                    value = {this.state.text}
                    onChange={this.change} 
                    style = {inputText}
                    placeholder= "add Task.." 
                />
                <Button text="Tambah" variant="primary" size="sm" action={this.submit}/>
            </form>
        );
    }
}

export default FormInput;