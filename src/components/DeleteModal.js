import React from 'react'
import PropTypes from "prop-types";
import Button from './Button'
import '../styles/DeleteModal.css'


class DeleteModal extends React.Component {
    render(){
        const {hapus, closeDelete, del, hapusData} = this.props;
        const delById = id => {
            del(id)
        }
        if(hapus) {
            return (
                <div className='modal'>
                    <div className='modal-body'>
                        <h3>Hapus data ini?</h3>
                        <div className="input">
                            <input type="hidden" value={hapusData.id} />
                            <input type="hidden" value={hapusData.title} />
                        </div>
                        <div className='btn-group'>
                            <Button text='Ya, hapus' variant='primary' action={() => delById(hapusData.id)}/>
                            <Button text='Batal' variant='danger' action={closeDelete}/>
                        </div>
                    </div>
                </div>
            )
        } else {
            return null
        }
    }
}

DeleteModal.propTypes= {
    del: PropTypes.func.isRequired
}

export default DeleteModal