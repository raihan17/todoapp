import React from 'react';
import PropTypes from "prop-types";
import Button from './Button';

import "../styles/Button.css";

const listItem = {
    background: "#21364C",
    color: "#fff",
    display: "flex",
    alignItems: "center",
    height: "3rem",
    padding: "0 1rem",
    justifyContent: "space-between",
    margin: "0.5rem 0",
    borderRadius: "4px"
}

const List = (props) => {
    const { todos, openEdit, openDelete } = props;
    // const delById = id => {
    //     del(id)
    // }
    return (
        <div style={listItem}>
           <p>{todos.title}</p>
           <div>
               <Button text="Edit" variant="default" size="sm" action={()=> openEdit(todos.id, todos.title)} />
               {/* <Button text="Hapus" variant="danger" size="sm" action={() => delById(todos.id)} /> */}
               <Button text="Hapus" variant="danger" size="sm" action={() => openDelete(todos.id)} />
           </div>
        </div>
    );
}

//props sebagai object
List.propTypes = {
    todos: PropTypes.object.isRequired,
    // del: PropTypes.func.isRequired 
}

export default List;