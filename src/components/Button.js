import React from 'react';
import PropTypes from "prop-types";


const Button = (props) => {

    const { text, variant, size, action } = props;

    return (
        <button className={`btn btn-${variant} btn-${size}`} onClick={action}>{text}</button>
    );
}

//jenis props yang dimiliki button
Button.propTypes = {
    text: PropTypes.string.isRequired,
    variant: PropTypes.string.isRequired,
    size: PropTypes.string,
    action: PropTypes.func,
}

export default Button;