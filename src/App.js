import React from "react";
import List from "./components/List";
import FormInput from "./components/FormInput";
import './App.css';
import todo from './todo.jpg';
import DeleteModal from "./components/DeleteModal";
import EditModal from "./components/EditModal";

class App extends React.Component {

  state = {
    todos: [
      {
        id: 1,
        title: "Reading book"
      },
      {
        id: 2,
        title: "Playing games"
      },
      {
        id: 3,
        title: "Watching Movie"
      },
    ],
    isEdit: false,
    editData: {
      id: "",
      title: ""
    },
    isDelete: false,
    deleteData: {
      id: "",
      title: ""
    }
  }

  /** membuat function event untuk edit mendapatkan data sesuai id yg di klik */
  setTitle = e => {
    this.setState({
      editData: {
        ...this.state.editData,
        title: e.target.value
      }
    })
  }

  openEditModal = (id, data) => {
    this.setState({
        isEdit: true,
        editData: {
          id: id,
          title: data
        }
    })
  }
  
  closeEditModal = () => {
    this.setState({
      isEdit: false
    })
  }

  closeDeleteModal = () => {
    this.setState({
      isDelete: false
    })
  }

  openDeleteModal = (id, data) => {
    this.setState({
      isDelete: true,
      deleteData: {
        id: id,
        title: data
      }
    })
  }

  addTask = data => {
    //console.log('add data..');
    const id = this.state.todos.length
    const newData = {
      id: id + 1,
      title: data
    }
    this.setState({
      todos: [...this.state.todos, newData] //krn datanya lebih dari satu, maka gunakan separete operator
    })
  }

  update = () => {
    const {id, title} = this.state.editData
    const newData = {id, title}
    const newTodos = this.state.todos
    newTodos.splice((id-1), 1, newData)
    this.setState({
      todos: newTodos,
      isEdit: false,
      editData: {
        id: "",
        title: ""
      }
    })
  }

  deleteTask = id => {
    this.setState({
      todos: this.state.todos.filter(item => item.id !== id),
      isDelete: false,
      // deleteData: {
      //   id: "",
      //   title: ""
      // }
    })
    alert('Delete success..');
    console.log('Delete ..');
  }

  render(){
    const { todos } = this.state;
    return (
      <div className="app">
        <div className="todo">
          <img src={todo} alt="logo" />
          <h3>Task List</h3>
        </div>
        <div className="list">
          {
            todos.map(item =>
              <List 
                key={item.id} 
                todos={item} 
                // del={this.deleteTask}
                openEdit={this.openEditModal}
                openDelete = {this.openDeleteModal}
              />
            )
          }
        </div>
        <div className="input-form">
          <FormInput add={this.addTask} />
        </div>
        <EditModal 
          edit={this.state.isEdit} 
          closeEdit={this.closeEditModal} 
          change={this.setTitle}
          data = {this.state.editData}
          update = {this.update} 
        />
        <DeleteModal
          hapus={this.state.isDelete}
          closeDelete={this.closeDeleteModal}
          del={this.deleteTask}
          hapusData = {this.state.deleteData}
        /> 
      </div>
    );
  }
  
}

export default App;
